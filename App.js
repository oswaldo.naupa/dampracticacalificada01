/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as React from 'react';
import { Text, View, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Details from './app/components/Details/Details' 
import ConexionFetch from './app/components/conexionFetch/ConexionFetch'

function HomeScreen({ navigation }) {
  return (
    <ConexionFetch navigation={navigation}></ConexionFetch>
  );
}

function DetailsScreen({ route, navigation }) {
  /* 2. Get the param */
  const { title } = route.params;
  const { image } = route.params;
  const { summary } = route.params;

  const titulo = JSON.stringify(title)
  const imagen = JSON.stringify(image)
  const resumen = JSON.stringify(summary)

  return (
    <View>
      <Details titulo={titulo} navigation={navigation} imagen={imagen} resumen={resumen} ></Details>     
    </View>
  );
}
const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} 
        options={{
          title: 'Lista de pelculas',
          headerStyle: {
            backgroundColor: '#f4511e',

          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontFamily: 'Bangers-Regular'
          },
        }}
        />
        <Stack.Screen name="Details" component={DetailsScreen} 
        options={{
          title: 'Detalles pelicula',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
  
}
